
import '../styles/index.css';
import App from "./components/app";
import ReactDOM from "react-dom/client";


const container = document.getElementById("app");
ReactDOM.hydrateRoot(container, <App />);